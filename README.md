Part 1. Basic (4 points)

Change your system, global and repository config (add some values). Inspect how it is override.
Add some aliases in your own opinion.
Make init commit.
Create develop branch. Checkout on it.
Create index.html empty file. Commit.
Create branch with any name. Checkout on it. Add some images folder with images inside it. Commit.
Change your index.html. Add some image src inside it. Commit.
Go back to develop branch.
Create branch with any name. Checkout on it. Add some styles folder with styles source inside it. Commit.
Change your index.html. Add some styles inside it. Commit.
Go to develop branch.
Merge two new branches into develop using git merge command.
Don’t delete two new branches!
Merge develop into master
Repeat 6-15 items once more, but now use git rebase command for merging. Please, create situation with conflicts. It is will be better for your understanding.
Try to inspect your repository with git log command. Use different options with this command (git log --help)
Push all your changes with all your branches to remote repository.
Part 2. Advanced (6 points)

Checkout to master branch. Commit README document with any words or sentences.
Checkout to develop branch. Create base file (java) and add content inside (class with any name and one method “public int add(int a, int b)” with add operation implementation). Commit it to develop branch.
Create otherOperation branch.
Add sub (subtraction) implementation (new public method like on previous step). Commit.
Add div(division) implementation. Commit.
Add mult (multiplication) implementation. Commit.
Add sqrt implementation. Step by step. a. add method with implementation to your java file. b. add changed file to staging zone. c. get your staged file back to working directory (think how) d. change sqrt method name to sqrtBad. e. add to staging zone f. commit
Reset all your repo (read about reset command and all possible options of resetting) to mult implementation commit (i.e remove last commit with sqrtBad method)
Add sqrt implementation. Commit.
Rebase it to develop branch. Squash it to 1 commit. Write good and clear message.
Checkout to develop. Create new branch advancedOperation.
Add implementation for add operation with 2 parameters. The first line of method “System out (“This is add method”)”. Commit.
Add implementation for add operation with 3 parameters. The first line of method “System out (“This is add method for 3 params”)”. The first line of method add with 2 parameters “System out (“This is add method for 2 params”)”. Commit.
Add implementation for add operation with 4 parameters. The first line of method “System out (“This is add method for 4 params”)”. Commit.
Checkout to your “grandparent commit”. Copy first line of add method for 2 params. Checkout back to the “tail” of your current branch and paste line to add method with 2 params. Commit.
Merge it with develop branch.
Merge develop with master.
Push your changes to remote repository with all refs (branches).
When you are done with it – get your reflog, attach it to the result mail and send it to your mentor.